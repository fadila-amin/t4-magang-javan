<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::resource('employee', EmployeeController::class);
Route::resource('company', CompanyController::class);
Route::get('/emplo/export/', 'EmployeeController@export')->name('exportemployee');

Route::view('/', 'app');
Route::view('/desa', 'app');
//Provinsi Route
Route::view('/provinsi', 'app');
Route::view('/provinsi/create', 'app');
Route::view('/provinsi/{id}/edit', 'app');
Route::view('/provinsi/{id}/update', 'app');
Route::view('/provinsi/{id}/delete', 'app');
//Kabupaten Route
Route::view('/kabupaten', 'app');
Route::view('/kecamatan', 'app');
Route::view('/reservasi', 'app');
Route::view('/film', 'app');
Route::view('/pesanfilm', 'app');
Route::view('/people', 'app');
