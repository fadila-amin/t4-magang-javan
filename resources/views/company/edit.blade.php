<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Edit Company</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">

</head>

<body>
<div class="container py-3">
    <div class="card w-50 mx-auto">
        <h5 class="card-header">Edit Company</h5>
        <div class="card-body">
            <form action="{{ route('company.update',$company->id) }}" method="POST">
                @csrf
                @method('put')
                <div class="form-group mb-3">
                    <label for="nama">Nama :</label>
                    <input type="text" id="nama" class="form-control" name="nama" value="{{$company->nama}}" />
                </div>
                <div class="form-group mb-3">
                    <label for="">Alamat</label>
                    <input type="text" class="form-control" name="alamat" placeholder="Alamat" value="{{$company->alamat}}">
                </div>
                <div class="form-group mb-3 d-grid">
                    <input type="submit" class="btn btn-primary" value="simpan">
                </div>
            </form>
        </div>
    </div>
</div>
</body>

</html>
