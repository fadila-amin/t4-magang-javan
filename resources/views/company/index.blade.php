<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Company</title>

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">
</head>

<body>

    <div class="container py-3">
        @if ($message = Session::get('success'))
            <div class="alert alert-success" role="alert">
                {{$message}}
            </div>
        @endif

        <div class="card">
        <div class="card-header d-flex justify-content-between align-items-center">
            <h3 class="m-0 p-0">List Company</h3>

            <div class="wrapper-button">
                <a href="{{ route('company.create') }}" class="btn btn-primary btn-sm">Tambah</a>
            </div>
        </div>
        <div class="card-body">
            <table class="table table-bordered table-hover">
                <thead>
                <tr>
                    <th class="text-center">no</th>
                    <th>nama</th>
                    <th class="text-center">alamat</th>
                    <th class="text-center" style="width: 200px">aksi</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($company as $no => $item)
                    <tr>
                        <td class="text-center">{{ $no+1 }}.</td>
                        <td>{{ $item->nama }}</td>
                        <td class="text-center">{{ $item->alamat }}</td>
                        <td class="d-flex justify-content-center">
                            <a href="{{ route('company.edit', $item->id) }}" class="mx-2 btn btn-warning btn-sm">Edit</a>
                            <form action="{{ route('company.destroy', $item->id) }}" method="POST">
                                @csrf
                                <input type="hidden" name="_method" value="delete"/>
                                <input type="submit" class="btn btn-sm btn-danger" value="Hapus">
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
    </div>
</body>

</html>
