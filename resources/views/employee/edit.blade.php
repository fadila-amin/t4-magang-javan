<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Edit Employee</title>

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">

</head>

<body>
<div class="container py-3">
    <div class="card w-50 mx-auto">
        <h5 class="card-header">Tambah Employe</h5>
        <div class="card-body">
            <form action="{{ route('employee.update',$employee->id) }}" method="POST">
                @csrf
                @method('put')
                <div class="form-group mb-3">
                    <label for="nama">Nama :</label>
                    <input type="text" id="nama" class="form-control" name="nama" value="{{$employee->nama}}" placeholder="Bambang Sutisna">
                </div>
                <div class="form-group mb-3">
                    <label for="atasan">Atasan :</label>
                    <select name="atasan_id" class="form-control" id="atas">
                        <option value="null">tidak memiliki atasan</option>
                        @foreach ($atasan as $item)
                            <option @if ($employee->atasan_id == $item->id)
                                    selected
                                    @endif value="{{ $item->id }}">{{ $item->nama }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group mb-3">
                    <label for="company">Company :</label>
                    <select name="company_id" id="atas" class="form-control">
                        @foreach ($company as $item)
                            <option @if ($employee->company_id == $item->id)
                                    selected
                                    @endif value="{{ $item->id }}">{{ $item->nama }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group mb-3 d-grid">
                    <input type="submit" class="btn btn-primary" value="simpan">
                </div>
            </form>
        </div>
    </div>
</div>

{{--  Js  --}}
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-U1DAWAznBHeqEIlVSCgzq+c9gqGAJn5c/t99JyeKa9xxaYpSvHU5awsuZVVFIhvj"
        crossorigin="anonymous"></script>
</body>

</html>
