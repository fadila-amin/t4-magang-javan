<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Employee</title>

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">
</head>

<body>

<div class="container py-3">
    @if ($message = Session::get('success'))
        <div class="alert alert-success" role="alert">
            {{$message}}
        </div>
    @endif

    <div class="card">
        <div class="card-header d-flex justify-content-between align-items-center">
            <h3 class="m-0 p-0">List Employes</h3>

            <div class="wrapper-button">
                <a href="{{ route('employee.create') }}" class="btn btn-primary btn-sm">Tambah</a>
            </div>
        </div>
        <div class="card-body">
            <table class="table table-bordered table-hover">
                <thead>
                <tr>
                    <th>id</th>
                    <th>nama</th>
                    <th class="text-left">posisi</th>
                    <th class="text-left">perusahaan</th>
                    <th class="text-center" style="width: 200px">aksi</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($employee as $no => $item)
                    <tr>
                        <td>{{ $item->id }}</td>
                        <td>{{ $item->nama }}</td>
                        @if ($item->atasan)
                            <td class="text-left">{{ $item->atasan->nama }}</td>
                        @else
                            <td class="text-left">-</td>
                        @endif
                        <td class="text-left">{{ $item->company->nama }}</td>
                        <td class="d-flex justify-content-center">
                            <a href="{{ route('employee.edit', $item->id) }}" class="mx-2 btn btn-warning btn-sm">Edit</a>
                            <form action="{{ route('employee.destroy', $item->id) }}" method="POST">
                                @csrf
                                <input type="hidden" name="_method" value="delete"/>
                                <input type="submit" class="btn btn-sm btn-danger" value="Hapus">
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>

            <div class="wrapper-export-button">
                <a href="{{route('exportemployee')}}?type=pdf" class="btn btn-sm btn-warning px-3 font-weight-bold mx-2">Export data by pdf</a>
                <a href="{{route('exportemployee')}}?type=excel" class="btn btn-sm btn-info px-3 font-weight-bold mx-2">Export data by excel</a>
            </div>
        </div>
    </div>
</div>

{{-- Js    --}}
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-U1DAWAznBHeqEIlVSCgzq+c9gqGAJn5c/t99JyeKa9xxaYpSvHU5awsuZVVFIhvj"
        crossorigin="anonymous"></script>
</body>

</html>
