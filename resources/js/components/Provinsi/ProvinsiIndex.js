import React, {useEffect, useState} from "react";
import {Link} from "react-router-dom"
import axios from "axios";
import {AlertSuccess} from "../utils/Alert";

const ProvinsiIndex = () => {
    const [provincies, setProvincies] = useState([]);
    const [success, setSuccess] = useState(false);
    const [error, setError] = useState("");

    useEffect(() => {
        const fetchProvincies = async () => {
            setError("");
            try {
                const {data} = await axios.get('/api/provinsi');
                setProvincies(data.data);
            } catch (err) {
                setError(err.message);
            }
        }

        fetchProvincies();
    }, [provincies]);

    const handleDelete = async (e) => {
        setSuccess(false);
        await axios.delete(`/api/provinsi/${e.target.dataset.id}`);
        setSuccess(true);
    }

    return (
        <div>
            {(success) && (<AlertSuccess />)}
            <div className="card">
                <div className="card-header d-flex justify-content-between align-items-center">
                    <h5 className="m-0 p-0">List Provinsi</h5>
                    <div className="button-wrapper">
                        <Link to="/provinsi/create" className="btn btn-sm btn-info">Tambah</Link>
                    </div>
                </div>
                <ul className="list-group list-group-flush">
                    {(provincies.length) ?
                        provincies.map((provinsi, i) => (
                            <li className="list-group-item d-flex justify-content-between align-items-center" key={provinsi.id}>
                                <span>{provinsi.nama}</span>
                                <div className="wrapper-button">
                                    <Link to={`/provinsi/${provinsi.id}/edit`} className="btn btn-sm btn-warning mx-1">Edit</Link>
                                    <button
                                        className="btn btn-sm btn-danger mx-1"
                                        onClick={handleDelete}
                                        data-id={provinsi.id}
                                    >Hapus</button>
                                </div>
                            </li>
                        ))
                        : (
                            <li className="list-group-item d-flex justify-content-center">Tidak Ada Data</li>
                        )}
                </ul>
            </div>
        </div>
    )
}

export default ProvinsiIndex;
