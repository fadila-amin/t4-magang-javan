import React, {useState} from "react";
import axios from "axios";
import {useHistory} from "react-router-dom";

import {AlertError} from "../utils/Alert";

const ProvinsiInput = () => {
    const [provinsi, setProvinsi] = useState("");
    const [error, setError] = useState(false);
    const histroy = useHistory();

    const handleSubmit = async (e) => {
        e.preventDefault();
        setError(false);
        try {
            const resp = await axios.post('/api/provinsi', {nama: provinsi});
            histroy.push({pathname:"/provinsi", state: {alert: "success"}});
            setError(false);
        } catch (err) {
            setError(true);
        }
    }
    return (
        <div>
            {(error) && (<AlertError/>)}
            <div className="card mx-auto" style={{width: "18rem"}}>
                <div className="card-header d-flex align-items-center">
                    <h5 className="m-0 p-0">Provinsi</h5>
                </div>
                <div className="card-body">
                    <form onSubmit={handleSubmit}>
                        <div className="form-group">
                            <label htmlFor="desa">Nama Provinsi:</label>
                            <input
                                type="text"
                                id="desa"
                                className="form-control"
                                placeholder="Temanggung"
                                value={provinsi}
                                onChange={e => setProvinsi(e.target.value)}
                            />
                        </div>
                        <div className="form-group">
                            <input type="submit" className="btn btn-sm btn-primary btn-block" value="Submit"/>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    )
}

export default ProvinsiInput;
