import React, {useEffect, useState} from "react";
import {useHistory, useParams} from "react-router-dom"
import axios from "axios";


const ProvinsiEdit = () => {
    const [provinsi, setProvinsi] = useState("");
    const [prov, setProv] = useState({});
    const {id} = useParams();
    const histroy = useHistory();

    useEffect(  () => {
        const fetchProvinsi = async  () => {
            try {
                const {data} = await axios.get(`/api/provinsi/${id}`);
                setProv(data.data);
                setProvinsi(data.data.nama);
            } catch (err) {
                console.log(err.message);
            }
        }
        fetchProvinsi();
    }, [id]);

    const handleSubmit = async (e) => {
        e.preventDefault();
        try {
            await axios.put(`/api/provinsi/${id}`, {nama: provinsi});
            histroy.push({pathname:"/provinsi", state: {alert: "success"}});
        } catch (err) {
            console.log(err.message);
        }
    }

    return (
        <div>
            {(prov) ? (
                <div className="card mx-auto" style={{width: "18rem"}}>
                    <div className="card-header d-flex align-items-center">
                        <h5 className="m-0 p-0">Provinsi</h5>
                    </div>
                    <div className="card-body">
                        <form onSubmit={handleSubmit}>
                            <div className="form-group">
                                <label htmlFor="desa">Nama Provinsi:</label>
                                <input
                                    type="text"
                                    id="desa"
                                    className="form-control"
                                    placeholder="Temanggung"
                                    value={provinsi}
                                    onChange={e => setProvinsi(e.target.value)}
                                />
                            </div>
                            <div className="form-group">
                                <input type="submit" className="btn btn-sm btn-primary btn-block" value="Submit"/>
                            </div>
                        </form>
                    </div>
                </div>
            ) : (
                <p>Tidak Ada Data</p>
            )}
        </div>
    )
}

export default ProvinsiEdit;
