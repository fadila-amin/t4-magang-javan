import React, {useEffect, useState} from "react";

const KecamatanInput = ({kabupatenAll, submit, id, data = null, input, close}) => {
    const [kecamatan, setKecamatan] = useState({
        nama: "",
        kabupaten_id: 0
    });

    useEffect(() => {
        if (data) {
           setKecamatan({nama: data.nama, kabupaten_id: data.kabupaten_id})
        }
    }, [data]);

    const handleChangeInput = (e) => {
        setKecamatan({...kecamatan, [e.target.name]: e.target.value});
    }
    return (
        <div>
            <div className="card mx-auto" >
                <div className="card-header d-flex justify-content-between align-items-center">
                    <h5 className="m-0 p-0">List Kecamatan</h5>
                    <div className="button-wrapper">
                        <button className="btn btn-sm btn-info" onClick={close}>Close</button>
                    </div>
                </div>
                <div className="card-body">
                    <form onSubmit={(e) => {
                        e.preventDefault();
                        (input !== "update") ? submit(kecamatan) : submit(id, kecamatan);
                    }}>
                        <div className="form-group">
                            <label htmlFor="desa">Nama Kecamatan:</label>
                            <input
                                type="text"
                                id="desa"
                                className="form-control"
                                placeholder="Doro"
                                name="nama"
                                value={kecamatan.nama}
                                onChange={handleChangeInput}
                            />
                        </div>
                        <div className="form-group">
                            <label htmlFor="desa">Nama Kabupaten:</label>
                            <select
                                name="kabupaten_id"
                                className="custom-select"
                                value={kecamatan.kabupaten_id}
                                onChange={handleChangeInput}>
                                <option>Pilih kabupaten</option>
                                {(kabupatenAll.length) ? (
                                    kabupatenAll.map((kabupaten, i) => (
                                        <option key={i} value={kabupaten.id}>{kabupaten.nama}</option>
                                    ))
                                ) : (
                                    <option>Tidak ada provinsi</option>
                                )}
                            </select>
                        </div>
                        <div className="form-group">
                            <input type="submit" className="btn btn-sm btn-primary btn-block" value="Submit"/>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    )
}

export default KecamatanInput;
