import React, {useEffect, useState} from "react";
import {
    addKecamatan, deleteKabupaten, deleteKecamatan,
    getKabupatenAll,
    getKecamatanAll,
    getSingleKabupaten,
    getSingleKecamatan,
    updateKabupaten, updateKecamatan
} from "../apis/api";
import KecamatanInput from "./KecamatanInput";

const KecamatanIndex = () => {
    const [kecamatanAll, setKecamatanAll] = useState([]);
    const [kecamatan, setKecamatan] = useState(null);
    const [kabupatenAll, setKabupatenAll] = useState([]);
    const [showList, setShowList] = useState(true);
    const [showAdd, setShowAdd] = useState(false);
    const [showUpdate, setShowUpdate] = useState(false);
    const [id, setId] = useState("");

    useEffect(() => {
        getKecamatanAll().then(data => setKecamatanAll(data));
        getKabupatenAll().then(data => setKabupatenAll(data));
    }, [showList]);

    const handleSubmitForm = async (data) => {
        const result = await addKecamatan(data);
        console.log(result);
        if(result.status === 201){
            setShowList(true);
            setShowAdd(false);
            setShowUpdate(false);
        }
    }

    const handleUpdateForm = async (dataid, datakecamatan) => {
        const result = await updateKecamatan(dataid, datakecamatan);
        if (result.status === 200) {
            setShowList(true);
            setShowUpdate(false);
            setShowAdd(false);
        }
    }

    const handleDeleteKecamatan = async (id) => {
        const res = await deleteKecamatan(id);
        setShowList(false);
        if (res.status === 200) {
            setShowList(true);
        }
    }

    const handleShowButton = () => {
        setShowAdd(true);
        setShowList(false);
    }

    const handleEditButton = async (idItem) => {
        const result = await getSingleKecamatan(idItem);
        setKecamatan(result);
        setId(idItem);
        setShowList(false);
        setShowUpdate(true);
    }

    const handleCloseButton = () => {
        setShowList(true);
        setShowAdd(false);
        setShowUpdate(false);
    }

    return (
        <div>
            {(showList) && (
                <div className="card">
                    <div className="card-header d-flex justify-content-between align-items-center">
                        <h5 className="m-0 p-0">List Kecamatan</h5>
                        <div className="button-wrapper">
                            <button className="btn btn-sm btn-info" onClick={handleShowButton} >Tambah</button>
                        </div>
                    </div>
                    <ul className="list-group list-group-flush">
                        <table className="table table-bordered">
                            <thead>
                            <tr>
                                <th className="text-center align-middle" style={{width: "100px"}}>No.</th>
                                <th>Kecamatan</th>
                                <th className="text-center align-middle">Kabupaten</th>
                                <th className="text-center align-middle" style={{width: "150px"}}>Aksi</th>
                            </tr>
                            </thead>
                            <tbody>
                            {(kecamatanAll.length) ? (
                                kecamatanAll.map((kecamatan, i) => (
                                    <tr key={i}>
                                        <th className="text-center align-middle">{i+1}</th>
                                        <td>{kecamatan.nama}</td>
                                        <td className="text-center align-middle">
                                            {
                                                kabupatenAll.map(kabupaten => {
                                                    if (kabupaten.id === kecamatan.kabupaten_id) {
                                                        return <span key={kabupaten.id}>{kabupaten.nama}</span>
                                                    }
                                                })
                                            }
                                        </td>
                                        <td className="d-flex align-items-center justify-content-center">
                                            <button
                                                className="btn btn-sm btn-warning mx-1"
                                                onClick={() => handleEditButton(kecamatan.id)}
                                            >Edit</button>
                                            <button
                                                className="btn btn-sm btn-danger mx-1"
                                                onClick={() => handleDeleteKecamatan(kecamatan.id)}
                                            >Delete</button>
                                        </td>
                                    </tr>
                                ))
                            ) : (
                                <tr>
                                    <td colSpan={4} className="text-center">Tidak Ada Data</td>
                                </tr>
                            )}
                            </tbody>
                        </table>
                    </ul>
                </div>
            )}
            {(showUpdate && kecamatan) && (<KecamatanInput input="update" id={id} data={kecamatan} submit={handleUpdateForm} kabupatenAll={kabupatenAll} close={handleCloseButton} />)}
            {(showAdd) && <KecamatanInput input="submit" submit={handleSubmitForm} kabupatenAll={kabupatenAll} close={handleCloseButton} />}
        </div>
    )
}

export default KecamatanIndex;
