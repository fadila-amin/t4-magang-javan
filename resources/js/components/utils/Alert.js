import React from "react";

export const AlertError = () => {
    return (
        <div className="mx-auto" style={{width: "18rem"}}>
            <div className="alert alert-danger" role="alert" >
                Gagal Menginputkan Data
            </div>
        </div>
    )
}

export const AlertSuccess = () => {
    return (
        <div className="mx-auto" style={{width: "18rem"}}>
            <div className="alert alert-success" role="alert" >
                Berhasil Menghapuskan Data
            </div>
        </div>
    )
}


