import axios from "axios";
import {useHistory} from "react-router-dom";

export async function getProvinsiAll() {
    try {
        const {data} = await axios.get('/api/provinsi');
        return data.data;
    } catch (err) {
        console.log(err.message);
    }
}

export async function getKabupatenAll() {
    try {
     const {data} = await axios.get('/api/kabupaten');
     return data.data;
    } catch (err) {
        console.log(err.message);
    }
}

export async function getSingleKabupaten(id) {
    try {
        const {data} = await axios.get(`/api/kabupaten/${id}`);
        return data.data;
    } catch (err) {
        console.log(err.message);
    }
}

export async function addKabupaten(data) {
    try {
        return await axios.post('/api/kabupaten', data);
    } catch (err) {
        console.log(err.message);
    }
}

export async function updateKabupaten(id, dataNew) {
    try {
        return await  axios.put(`/api/kabupaten/${id}`, dataNew);
    } catch (err) {
        console.log(err.message);
    }
}

export async function deleteKabupaten(id) {
    try {
        return await axios.delete(`/api/kabupaten/${id}`);
    } catch (err) {
        console.log(err.message);
    }
}

export async function getKecamatanAll() {
    try {
        const {data} = await axios.get('/api/kecamatan');
        return data.data;
    } catch (err) {
        console.log(err.message);
    }
}

export async function getSingleKecamatan(id) {
    try {
        const {data} = await axios.get(`/api/kecamatan/${id}`);
        return data.data;
    } catch (err) {
        console.log(err.message);
    }
}

export async function addKecamatan(data) {
    try {
        return await axios.post('/api/kecamatan', data);
    } catch (err) {
        console.log(err.message);
    }
}

export async function updateKecamatan(id, data) {
    try {
        return await  axios.put(`/api/kecamatan/${id}`, data);
    } catch (err) {
        console.log(err.message);
    }
}

export async function deleteKecamatan(id) {
    try {
        return await axios.delete(`/api/kecamatan/${id}`);
    } catch (err) {
        console.log(err.message);
    }
}

//Desa
export async function getDesaAll() {
    try {
        const {data} = await axios.get('/api/desa');
        return data.data;
    } catch (err) {
        console.log(err.message);
    }
}

export async function getSingleDesa(id) {
    try {
        const {data} = await axios.get(`/api/desa/${id}`);
        return data.data;
    } catch (err) {
        console.log(err.message);
    }
}

export async function addDesa(data) {
    try {
        return await axios.post('/api/desa', data);
    } catch (err) {
        console.log(err.message);
    }
}

export async function updateDesa(id, data) {
    try {
        return await  axios.put(`/api/desa/${id}`, data);
    } catch (err) {
        console.log(err.message);
    }
}

export async function deleteDesa(id) {
    try {
        return await axios.delete(`/api/desa/${id}`);
    } catch (err) {
        console.log(err.message);
    }
}

//People
export async function getPeopleAll() {
    try {
        const {data} = await axios.get('/api/penonton');
        return data.data;
    } catch (err) {
        console.log(err.message);
    }
}

export async function getSinglePeople(id) {
    try {
        const {data} = await axios.get(`/api/penonton/${id}`);
        return data;
    } catch (err) {
        console.log(err.message);
    }
}

export async function addPeople(data) {
    try {
        return await axios.post('/api/penonton', {nama: data});
    } catch (err) {
        console.log(err.message);
    }
}

export async function updatePeople(id, data) {
    try {
        return await  axios.put(`/api/penonton/${id}`, {nama: data});
    } catch (err) {
        console.log(err.message);
    }
}

export async function deletePeople(id) {
    try {
        return await axios.delete(`/api/penonton/${id}`);
    } catch (err) {
        console.log(err.message);
    }
}

// Film
export async function getFilmAll() {
    try {
        const {data} = await axios.get('/api/film');
        return data.data;
    } catch (err) {
        console.log(err.message);
    }
}

export async function getSingleFilm(id) {
    try {
        const {data} = await axios.get(`/api/film/${id}`);
        return data.data;
    } catch (err) {
        console.log(err.message);
    }
}

export async function addFilm(data) {
    try {
        return await axios.post('/api/film', data);
    } catch (err) {
        console.log(err.message);
    }
}

export async function updateFilm(id, data) {
    try {
        return await  axios.put(`/api/film/${id}`, data);
    } catch (err) {
        console.log(err.message);
    }
}

export async function deleteFilm(id) {
    try {
        return await axios.delete(`/api/film/${id}`);
    } catch (err) {
        console.log(err.message);
    }
}

//Reservasi FIlm
export async function addPesanFilm(data) {
    try {
        return await axios.post('/api/reservasi', data);
    } catch (err) {
        console.log(err.message);
    }
}

export async function deletePesanFilm(penonton_id, film_id) {
    try {
        return await axios.post('/api/batal-reservasi', {penonton_id: penonton_id, film_id: film_id});
    } catch (err) {
        console.log(err.message);
    }
}


