import React, {useState, useEffect} from "react";
import PeopleInput from "./PeopleInput";
import {
    addPeople,
    deletePeople,
    getPeopleAll, getSinglePeople, updatePeople
} from "../apis/api";

const PeopleIndex = () => {
    const [peoples, setPeoples] = useState([]);
    const [people, setPeople] = useState(null);
    const [showList, setShowList] = useState(true);
    const [showAdd, setShowAdd] = useState(false);
    const [showUpdate, setShowUpdate] = useState(false);
    const [id, setId] = useState("");

    useEffect(() => {
        getPeopleAll().then(data => setPeoples(data));
    }, [showList]);

    const handleSubmitForm = async (data) => {
        const result = await addPeople(data);
        if(result.status === 201){
            setShowList(true);
            setShowAdd(false);
            setShowUpdate(false);
        }
    }

    const handleUpdateForm = async (dataid, datapeople) => {
        const result = await updatePeople(dataid, datapeople);
        if (result.status === 200) {
            setShowList(true);
            setShowUpdate(false);
            setShowAdd(false);
        }
    }

    const handleDeletePeople = async (id) => {
        const res = await deletePeople(id);
        setShowList(false);
        if (res.status === 200) {
            setShowList(true);
        }
    }

    const handleShowButton = () => {
        setShowAdd(true);
        setShowList(false);
    }

    const handleCloseButton = () => {
        setShowList(true);
        setShowAdd(false);
        setShowUpdate(false);
    }

    const handleEditButton = async (idItem) => {
        const result = await getSinglePeople(idItem);
        setPeople(result.data);
        setId(idItem);
        setShowList(false);
        setShowUpdate(true);
    }


    return (
        <div>
            {(showList) && (
                <div className="card">
                    <div className="card-header d-flex justify-content-between align-items-center">
                        <h5 className="m-0 p-0">List People</h5>
                        <div className="button-wrapper">
                            <button className="btn btn-sm btn-info" onClick={handleShowButton}>Tambah</button>
                        </div>
                    </div>
                    <div className="card-body">
                        <table className="table table-bordered">
                            <thead>
                            <tr>
                                <th className="text-center align-middle" style={{width: "100px"}}>No.</th>
                                <th>Nama</th>
                                <th className="text-center" style={{width: "150px"}}>Aksi</th>
                            </tr>
                            </thead>
                            <tbody>
                            {(peoples.length) ? (
                                peoples.map((people, i) => (
                                    <tr key={people.id}>
                                        <th className="text-center align-middle">{i+1}</th>
                                        <td className="align-middle">{people.nama}</td>
                                        <td className="d-flex align-items-center justify-content-center">
                                            <button
                                                className="btn btn-sm btn-warning mx-1"
                                                onClick={() => handleEditButton(people.id)}
                                            >Edit</button>
                                            <button
                                                className="btn btn-sm btn-danger mx-1"
                                                onClick={() => handleDeletePeople(people.id)}
                                            >Delete</button>
                                        </td>
                                    </tr>
                                ))
                            ) : (
                                <tr>
                                    <td colSpan={3} className="text-center">Tidak Ada Data</td>
                                </tr>
                            )}
                            </tbody>
                        </table>
                    </div>
                </div>
            )}
            {(showUpdate && people) && (<PeopleInput input="update" submit={handleUpdateForm} data={people} id={id} close={handleCloseButton} />)}
            {(showAdd) && (<PeopleInput input="submit" submit={handleSubmitForm} close={handleCloseButton} />)}
        </div>
    )
}

export default PeopleIndex;
