import React, {useState, useEffect} from "react";

const PeopleInput = ({input, id, data, close, submit}) => {
    const [nama, setNama] = useState("");

    useEffect(() => {
        if (data) {
            setNama(data.nama)
        }
    }, [data]);

    const handleChangeInput = (e) => {
        setNama(e.target.value);
    }

    return (
        <div className="card mx-auto" >
            <div className="card-header d-flex justify-content-between align-items-center">
                <h5 className="m-0 p-0">People</h5>
                <div className="button-wrapper">
                    <button className="btn btn-sm btn-info" onClick={close}>Close</button>
                </div>
            </div>
            <div className="card-body">
                <form onSubmit={(e) => {
                    e.preventDefault();
                    (input !== "update") ? submit(nama) : submit(id, nama);
                }}>
                    <div className="form-group">
                        <label htmlFor="nama">Nama People:</label>
                        <input
                            type="text"
                            id="nama"
                            className="form-control"
                            placeholder="Bambang Susanto"
                            name="nama"
                            value={nama}
                            onChange={handleChangeInput}
                        />
                    </div>
                    <div className="form-group">
                        <input type="submit" className="btn btn-sm btn-primary btn-block" value="Submit"/>
                    </div>
                </form>
            </div>
        </div>
    )
}

export default PeopleInput;
