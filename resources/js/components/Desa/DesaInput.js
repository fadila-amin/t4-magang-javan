import React, {useEffect, useState} from "react";

const DesaInput = ({data = null, input, id, submit, close, kecamatanAll}) => {
    const [desa, setDesa] = useState({
        nama: "",
        kecamatan_id: 0
    });

    useEffect(() => {
        if (data) {
            setDesa({nama: data.nama, kecamatan_id: data.kecamatan_id});
        }
    }, [data])

    const handleChangeInput = (e) => {
        setDesa({...desa, [e.target.name]: e.target.value});
    }

    return (
        <div>
            <div className="card mx-auto">
                <div className="card-header d-flex justify-content-between align-items-center">
                    <h5 className="m-0 p-0">Desa</h5>
                    <div className="wrapper-button">
                        <button onClick={close} className="btn btn-sm btn-primary">Close</button>
                    </div>
                </div>
                <div className="card-body">
                    <form onSubmit={(e) => {
                        e.preventDefault();
                        (input !== "update") ? submit(desa) : submit(id, desa);
                    }}>
                        <div className="form-group">
                            <label htmlFor="desa">Nama Desa:</label>
                            <input
                                type="text"
                                id="desa"
                                className="form-control"
                                placeholder="Kaliketing"
                                name="nama"
                                value={desa.nama}
                                onChange={handleChangeInput}
                            />
                        </div>
                        <div className="form-group">
                            <label htmlFor="desa">Nama Kecamatan:</label>
                            <select
                                name="kecamatan_id"
                                className="custom-select"
                                value={desa.kecamatan_id}
                                onChange={handleChangeInput}>
                                <option>Pilih kecamatan</option>
                                {(kecamatanAll.length) ? (
                                    kecamatanAll.map((kecamatan, i) => (
                                        <option key={i} value={kecamatan.id}>{kecamatan.nama}</option>
                                    ))
                                ) : (
                                    <option>Tidak ada kecamatan</option>
                                )}
                            </select>
                        </div>
                        <div className="form-group">
                            <input type="submit" className="btn btn-sm btn-primary btn-block" value="Submit"/>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    )
}

export default DesaInput;
