import React, {useEffect, useState} from "react";
import {addDesa, deleteDesa, getDesaAll, getKecamatanAll, getSingleDesa, updateDesa} from "../apis/api";
import DesaInput from "./DesaInput";

const DesaIndex = () => {
    const [desa, setDesa] = useState(null);
    const [desaAll, setDesaAll] = useState([]);
    const [kecamatanAll, setKecamatanAll] = useState([]);
    const [showList, setShowList] = useState(true);
    const [showAdd, setShowAdd] = useState(false);
    const [showUpdate, setShowUpdate] = useState(false);
    const [id, setId] = useState("");

    useEffect(() => {
        getDesaAll().then(data => setDesaAll(data));
        getKecamatanAll().then(data => setKecamatanAll(data));
    }, [showList]);

    const hanldSubmitForm = async (data) => {
        const result = await addDesa(data);
        if (result.status === 201){
            setShowList(true);
            setShowAdd(false);
            setShowUpdate(false);
        }
    }

    const handleUpdateForm = async (dataid, dataDesa) => {
        const result = await updateDesa(dataid, dataDesa);
        if (result.status === 200) {
            setShowList(true);
            setShowUpdate(false);
            setShowAdd(false);
        }
    }

    const handleDeleteKabupaten = async (id) => {
        const res = await deleteDesa(id);
        setShowList(false);
        if (res.status === 200) {
            setShowList(true);
        }
    }

    const handleShowButton = () => {
        setShowAdd(true);
        setShowList(false);
    }

    const handleEditButton = async (idItem) => {
        const result = await getSingleDesa(idItem);
        setDesa(result);
        setId(idItem);
        setShowList(false);
        setShowUpdate(true);
    }

    const handleCloseButton = () => {
        setShowList(true);
        setShowAdd(false);
        setShowUpdate(false);
    }

    return (
        <div>
            {(showList) && (
                <div className="card">
                    <div className="card-header d-flex justify-content-between align-items-center">
                        <h5 className="m-0 p-0">List Desa</h5>
                        <div className="button-wrapper">
                            <button className="btn btn-sm btn-info" onClick={handleShowButton}>Tambah</button>
                        </div>
                    </div>
                    <ul className="list-group list-group-flush">
                        <table className="table table-bordered">
                            <thead>
                            <tr>
                                <th className="text-center align-middle" style={{width: "100px"}}>No.</th>
                                <th>Desa</th>
                                <th className="text-center align-middle">Kecamatan</th>
                                <th className="text-center align-middle" style={{width: "150px"}}>Aksi</th>
                            </tr>
                            </thead>
                            <tbody>
                            {(desaAll.length) ? (
                                desaAll.map((desa, i) => (
                                    <tr key={i}>
                                        <th className="text-center align-middle">{i+1}</th>
                                        <td>{desa.nama}</td>
                                        <td className="text-center align-middle">
                                            {
                                                kecamatanAll.map(kecamatan => {
                                                    if (kecamatan.id === desa.kecamatan_id) {
                                                        return <span key={kecamatan.id}>{kecamatan.nama}</span>
                                                    }
                                                })
                                            }
                                        </td>
                                        <td className="d-flex align-items-center justify-content-center">
                                            <button
                                                className="btn btn-sm btn-warning mx-1"
                                                onClick={() => handleEditButton(desa.id)}
                                            >Edit</button>
                                            <button
                                                className="btn btn-sm btn-danger mx-1"
                                                onClick={() => handleDeleteKabupaten(desa.id)}
                                            >Delete</button>
                                        </td>
                                    </tr>
                                ))
                            ) : (
                                <tr>
                                    <td colSpan={4} className="text-center">Tidak Ada Data</td>
                                </tr>
                            )}
                            </tbody>
                        </table>
                    </ul>
                </div>
            )}
            {(showUpdate && desa) && <DesaInput input="update" id={id} data={desa} submit={handleUpdateForm} kecamatanAll={kecamatanAll} close={handleCloseButton} />}
            {(showAdd) && <DesaInput input="submit" submit={hanldSubmitForm} kecamatanAll={kecamatanAll} close={handleCloseButton}/>}
        </div>
    )
}

export default DesaIndex;
