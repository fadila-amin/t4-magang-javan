import React, {useEffect, useState} from "react";

const FilmInput = ({data = null, input, id, submit, close}) => {
    const [film, setFilm] = useState({
        judul: "",
        tiket: 0
    });

    useEffect(() => {
        if (data) {
            setFilm({judul: data.judul, tiket: data.tiket});
        }
    }, [data])

    const handleChangeInput = (e) => {
        setFilm({...film, [e.target.name]: e.target.value});
    }

    return (
        <div>
            <div className="card mx-auto">
                <div className="card-header d-flex justify-content-between align-items-center">
                    <h5 className="m-0 p-0">Film</h5>
                    <div className="wrapper-button">
                        <button onClick={close} className="btn btn-sm btn-primary">Close</button>
                    </div>
                </div>
                <div className="card-body">
                    <form onSubmit={(e) => {
                        e.preventDefault();
                        (input !== "update") ? submit(film) : submit(id, film);
                    }}>
                        <div className="form-group">
                            <label htmlFor="film">Nama Film:</label>
                            <input
                                type="text"
                                id="film"
                                className="form-control"
                                placeholder="Contoh film"
                                name="judul"
                                value={film.judul}
                                onChange={handleChangeInput}
                            />
                        </div>
                        <div className="form-group">
                            <label htmlFor="tiket">Tiket:</label>
                            <input
                                type="text"
                                id="tiket"
                                className="form-control"
                                placeholder="40"
                                name="tiket"
                                value={film.tiket}
                                onChange={handleChangeInput}
                            />
                        </div>
                        <div className="form-group">
                            <input type="submit" className="btn btn-sm btn-primary btn-block" value="Submit"/>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    )
}

export default FilmInput;
