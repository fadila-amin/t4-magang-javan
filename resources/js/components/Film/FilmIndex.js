import React, {useEffect, useState} from "react";
import {addFilm, deleteFilm, getFilmAll, getSingleFilm, updateFilm} from "../apis/api";
import FilmInput from "./FilmInput";

const FilmIndex = () => {
    const [filmAll, setFilmAll] = useState([]);
    const [film, setFilm] = useState(null);
    const [showList, setShowList] = useState(true);
    const [showAdd, setShowAdd] = useState(false);
    const [showUpdate, setShowUpdate] = useState(false);
    const [id, setId] = useState("");

    useEffect(() => {
        getFilmAll().then(data => setFilmAll(data));
    }, [showList]);

    const hanldSubmitForm = async (data) => {
        const result = await addFilm(data);
        if (result.status === 201){
            setShowList(true);
            setShowAdd(false);
            setShowUpdate(false);
        }
    }

    const handleUpdateForm = async (dataid, datafilm) => {
        const result = await updateFilm(dataid, datafilm);
        if (result.status === 200) {
            setShowList(true);
            setShowUpdate(false);
            setShowAdd(false);
        }
    }

    const handleDeleteFilm = async (id) => {
        const res = await deleteFilm(id);
        setShowList(false);
        if (res.status === 200) {
            setShowList(true);
        }
    }

    const handleShowButton = () => {
        setShowAdd(true);
        setShowList(false);
    }

    const handleEditButton = async (idItem) => {
        const result = await getSingleFilm(idItem);
        setFilm(result);
        setId(idItem);
        setShowList(false);
        setShowUpdate(true);
    }

    const handleCloseButton = () => {
        setShowList(true);
        setShowAdd(false);
        setShowUpdate(false);
    }

    return (
        <div>
            {(showList) && (
                <div className="card">
                    <div className="card-header d-flex justify-content-between align-items-center">
                        <h5 className="m-0 p-0">List Film</h5>
                        <div className="button-wrapper">
                            <button className="btn btn-sm btn-info" onClick={handleShowButton}>Tambah</button>
                        </div>
                    </div>
                    <ul className="list-group list-group-flush">
                        <table className="table table-bordered">
                            <thead>
                            <tr>
                                <th className="text-center align-middle" style={{width: "100px"}}>No.</th>
                                <th>Film</th>
                                <th className="text-center align-middle">Tiket</th>
                                <th className="text-center align-middle" style={{width: "150px"}}>Aksi</th>
                            </tr>
                            </thead>
                            <tbody>
                            {(filmAll.length) ? (
                                filmAll.map((film, i) => (
                                    <tr key={i}>
                                        <th className="text-center align-middle">{i+1}</th>
                                        <td>{film.judul}</td>
                                        <td className="text-center align-middle">{film.tiket}</td>
                                        <td className="d-flex align-items-center justify-content-center">
                                            <button
                                                className="btn btn-sm btn-warning mx-1"
                                                onClick={() => handleEditButton(film.id)}
                                            >Edit</button>
                                            <button
                                                className="btn btn-sm btn-danger mx-1"
                                                onClick={() => handleDeleteFilm(film.id)}
                                            >Delete</button>
                                        </td>
                                    </tr>
                                ))
                            ) : (
                                <tr>
                                    <td colSpan={4} className="text-center">Tidak Ada Data</td>
                                </tr>
                            )}
                            </tbody>
                        </table>
                    </ul>
                </div>
            )}
            {(showUpdate && film) && <FilmInput input="update" id={id} data={film} submit={handleUpdateForm} close={handleCloseButton} />}
            {(showAdd) && <FilmInput input="submit" submit={hanldSubmitForm} close={handleCloseButton} />}
        </div>
    )
}

export default FilmIndex;
