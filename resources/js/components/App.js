import React from "react";
import ReactDOM from "react-dom";
import {BrowserRouter, Switch, Route} from "react-router-dom";

//Components
import ListPage from "./ListPage";
import Header from "./Header/Header";
import DesaIndex from "./Desa/DesaIndex";
import ProvinsiIndex from "./Provinsi/ProvinsiIndex";
import ProvinsiInput from "./Provinsi/ProvinsiInput";
import ProvinsiEdit from "./Provinsi/ProvinsiEdit";
import KabupatenIndex from "./Kabupaten/KabupatenIndex";
import KecamatanIndex from "./Kecamatan/KecamatanIndex";
import Reservasi from "./Reservasi/Reservasi";
import PeopleIndex from "./People/PeopleIndex";
import FilmIndex from "./Film/FilmIndex";
import PesanFilm from "./Pesan/PesanFilm";

const App = () => {
    return (
        <BrowserRouter>
            <div className="container bg-info p-5 border">
                <Header/>
                <Switch>
                    <Route path="/" exact component={ListPage} />
                    <Route path="/provinsi" exact component={ProvinsiIndex} />
                    <Route path="/provinsi/create" exact component={ProvinsiInput} />
                    <Route path="/provinsi/:id/edit" exact component={ProvinsiEdit} />
                    <Route path="/kabupaten" exact component={KabupatenIndex} />
                    <Route path="/kecamatan" exact component={KecamatanIndex} />
                    <Route path="/reservasi" exact component={Reservasi} />
                    <Route path="/people" exact component={PeopleIndex} />
                    <Route path="/film" exact component={FilmIndex} />
                    <Route path="/pesanfilm" exact component={PesanFilm} />
                    <Route path="/desa" exact component={DesaIndex} />
                </Switch>
            </div>
        </BrowserRouter>
    )
}

ReactDOM.render(<App />, document.getElementById('root'));
