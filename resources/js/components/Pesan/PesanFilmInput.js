import React, { useState} from "react";

const PesanFilmInput = ({submit, close, filmAll, peopleAll}) => {
    const [reservasi, setReservasi] = useState({
        film_id: 0,
        penonton_id: 0
    });

    const handleChangeInput = (e) => {
        setReservasi({...reservasi, [e.target.name]: e.target.value});
    }
    return (
        <div>
            <div className="card mx-auto">
                <div className="card-header d-flex justify-content-between align-items-center">
                    <h5 className="m-0 p-0">Buat Reservasi</h5>
                    <div className="wrapper-button">
                        <button onClick={close} className="btn btn-sm btn-primary">Close</button>
                    </div>
                </div>
                <div className="card-body">
                    <form onSubmit={(e) => {
                        e.preventDefault();
                        submit(reservasi);
                    }}>
                        <div className="form-group">
                            <label htmlFor="penonton_id">Nama Penonton:</label>
                            <select
                                name="penonton_id"
                                className="custom-select"
                                value={reservasi.penonton_id}
                                onChange={handleChangeInput}>
                                <option>Pilih penonton</option>
                                {(peopleAll.length) ? (
                                    peopleAll.map((people, i) => (
                                        <option key={i} value={people.id}>{people.nama}</option>
                                    ))
                                ) : (
                                    <option>Tidak ada penonton</option>
                                )}
                            </select>
                        </div>
                        <div className="form-group">
                            <label htmlFor="film_id">Judul Film:</label>
                            <select
                                name="film_id"
                                className="custom-select"
                                value={reservasi.film_id}
                                onChange={handleChangeInput}>
                                <option>Pilih penonton</option>
                                {(filmAll.length) ? (
                                    filmAll.map((film, i) => (
                                        <option key={i} value={film.id}>{film.judul}</option>
                                    ))
                                ) : (
                                    <option>Tidak ada film</option>
                                )}
                            </select>
                        </div>
                        <div className="form-group">
                            <input type="submit" className="btn btn-sm btn-primary btn-block" value="Submit"/>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    )
}

export default PesanFilmInput;
