import React, {useEffect, useState} from "react";
import {deletePesanFilm, getSinglePeople} from "../apis/api";
import {isEmpty} from "lodash/lang";

const ListFilmDelete = ({id}) => {
    const [people, setPeople] = useState({});
    const [alert, setAlert] = useState(false);

    useEffect(() => {
        getSinglePeople(id).then(data => setPeople(data));
    }, [alert]);

    console.log(people);

    const handleDeleteReservasiFilm = async (penonton_id, film_id) => {
        const res = await deletePesanFilm(penonton_id, film_id);
        console.log(res);
        if (res.status === 200) {
            setAlert(true);
            setTimeout(() => {
                setAlert(false);
            }, 3000)
        }
    }

    return (
            <div>
                {(alert) && (
                <div className="alert alert-success" role="alert">
                    Berhasil Menghapus Reservasi.
                </div>
                )}
                {(!isEmpty(people)) && (
                    <>
                        <h4>List Reservasi Film By {people.data.nama}</h4>
                        <table className="table table-bordered">
                            <thead>
                            <tr>
                                <th className="text-center" style={{width: "100px"}}>No.</th>
                                <th>Judul</th>
                                <th className="text-center" style={{width: "150px"}}>Aksi</th>
                            </tr>
                            </thead>
                            <tbody>
                            {(!isEmpty(people) && people.reservasi.length) ? (
                                people.reservasi.map((film, i) => (
                                        <tr key={i}>
                                            <td>{i+1}</td>
                                            <td>{film.judul}</td>
                                            <td className="text-center">
                                                <button
                                                    onClick={ () => handleDeleteReservasiFilm(people.data.id, film.id)}
                                                    className="btn btn-sm btn-danger"
                                                >Cancel</button>
                                            </td>
                                        </tr>
                                    )
                                )
                            ) : (
                                <tr>
                                    <td className="text-center" colSpan={3}>Tidak Ada Data</td>
                                </tr>
                            )}
                            </tbody>
                        </table>
                    </>
                )}
            </div>

    )
}

export default ListFilmDelete;
