import React, {useEffect, useState} from "react";
import {addPesanFilm, deletePesanFilm, getFilmAll, getPeopleAll} from "../apis/api";
import PesanFilmInput from "./PesanFilmInput";
import ListFilmDelete from "./ListFilmDelete";

const PesanFilm = () => {
    const [peopleAll, setPeopleAll] = useState([]);
    const [filmAll, setFilmAll] = useState([]);
    const [showList, setShowList] = useState(true);
    const [showAdd, setShowAdd] = useState(false);
    const [showDetail, setShowDetail] = useState(false);
    const [alert, setAlert] = useState(false);
    const [id, setId] = useState("");

    useEffect(() => {
        getPeopleAll().then(data => setPeopleAll(data));
        getFilmAll().then(data => setFilmAll(data));
    }, [showList]);

    const hanldSubmitForm = async (data) => {
        const result = await addPesanFilm(data);
        if (result.status === 200) {
            setShowList(true);
            setShowAdd(false);
            setAlert(true);
            setTimeout(() => setAlert(false), 3000);
        }
    }

    const handleShowButton = () => {
        setShowAdd(true);
        setShowList(false);
    }

    const handleDetailButton = (id) => {
        setId(id);
        setShowDetail(true);
        setShowList(false);
    }

    const handleCloseButton = () => {
        setShowList(true);
        setShowAdd(false);
        setShowCancel(false);
    }

    return (
        <div>
            {(showList) && (
                <>
                    {(alert) && (
                    <div className="alert alert-success" role="alert">
                        Berhasil membuat reservasi film
                    </div>
                    )}
                    <div className="card">
                        <div className="card-header d-flex justify-content-between align-items-center">
                            <h5 className="m-0 p-0">Reservasi Film</h5>
                            <div className="button-wrapper">
                                <button className="btn btn-sm btn-info" onClick={handleShowButton}>Tambah</button>
                            </div>
                        </div>
                        <ul className="list-group list-group-flush">
                            <table className="table table-bordered">
                                <thead>
                                <tr>
                                    <th>Nama</th>
                                    <th className="text-center" style={{width: "150px"}}>Aksi</th>
                                </tr>
                                </thead>
                                <tbody>
                                {(peopleAll.length) ? (
                                    peopleAll.map((people, i) => (
                                        <tr key={i}>
                                            <th>{people.nama}</th>
                                            <td className="text-center">
                                                <button className="btn btn-sm btn-info" onClick={() => handleDetailButton(people.id)}>Detail</button>
                                            </td>
                                        </tr>
                                    ))
                                ) : (
                                    <tr>
                                        <td colSpan={2}>Tidak ada data</td>
                                    </tr>
                                )}
                                </tbody>
                            </table>
                        </ul>
                    </div>
                </>
            )}
            {(showAdd) &&
            <PesanFilmInput submit={hanldSubmitForm} filmAll={filmAll} peopleAll={peopleAll}
                            close={handleCloseButton}/>}
            {(showDetail) && (
                <ListFilmDelete id={id} />
            )}
        </div>
    )
}

export default PesanFilm;
