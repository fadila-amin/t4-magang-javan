import React from "react";
import {Link} from "react-router-dom";

const Header = () => {
    return (
        <div>
            <header className="w-100 bg-info p-3">
                <Link to="/" className="btn btn-sm btn-warning">Home</Link>
            </header>
        </div>
    )
}

export default Header;
