import React from "react";

const ListPage = () => {
    return (
        <div className="list-group">
            <a href="/provinsi" className="list-group-item list-group-item-action list-group-item-success">Provinsi</a>
            <a href="/kabupaten" className="list-group-item list-group-item-action list-group-item-secondary">Kabupaten</a>
            <a href="/kecamatan" className="list-group-item list-group-item-action list-group-item-primary">Kecamatan</a>
            <a href="/desa" className="list-group-item list-group-item-action list-group-item-yellow">Desa</a>
            <a href="/reservasi" className="list-group-item list-group-item-action list-group-item-danger">Reservasi</a>
        </div>
    )
}

export default ListPage;
