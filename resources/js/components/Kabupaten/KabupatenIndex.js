import React, {useEffect, useState} from "react";
import {
    addKabupaten,
    deleteKabupaten,
    getKabupatenAll,
    getProvinsiAll,
    getSingleKabupaten,
    updateKabupaten
} from "../apis/api";
import KabupatenInput from "./KabupatenInput";

const KabupatenIndex = () => {
    const [kabupatenAll, setKabupatenAll] = useState([]);
    const [kabupaten, setKabupaten] = useState(null);
    const [provinsiAll, setProvinsiAll] = useState([]);
    const [showList, setShowList] = useState(true);
    const [showAdd, setShowAdd] = useState(false);
    const [showUpdate, setShowUpdate] = useState(false);
    const [id, setId] = useState("");

    useEffect(() => {
        getKabupatenAll().then(data => setKabupatenAll(data));
        getProvinsiAll().then(data => setProvinsiAll(data));
    }, [showList]);

    const hanldSubmitForm = async (data) => {
        const result = await addKabupaten(data);
        if (result.status === 201){
            setShowList(true);
            setShowAdd(false);
            setShowUpdate(false);
        }
    }

    const handleUpdateForm = async (dataid, datakabupaten) => {
        const result = await updateKabupaten(dataid, datakabupaten);
        if (result.status === 200) {
            setShowList(true);
            setShowUpdate(false);
            setShowAdd(false);
        }
    }

    const handleDeleteKabupaten = async (id) => {
        const res = await deleteKabupaten(id);
        setShowList(false);
        if (res.status === 200) {
            setShowList(true);
        }
    }

    const handleShowButton = () => {
        setShowAdd(true);
        setShowList(false);
    }

    const handleEditButton = async (idItem) => {
        const result = await getSingleKabupaten(idItem);
        setKabupaten(result);
        setId(idItem);
        setShowList(false);
        setShowUpdate(true);
    }

    const handleCloseButton = () => {
        setShowList(true);
        setShowAdd(false);
        setShowUpdate(false);
    }

    return (
        <div>
            {(showList) && (
                <div className="card">
                    <div className="card-header d-flex justify-content-between align-items-center">
                        <h5 className="m-0 p-0">List Kabupaten</h5>
                        <div className="button-wrapper">
                            <button className="btn btn-sm btn-info" onClick={handleShowButton}>Tambah</button>
                        </div>
                    </div>
                    <ul className="list-group list-group-flush">
                        <table className="table table-bordered">
                            <thead>
                            <tr>
                                <th className="text-center align-middle" style={{width: "100px"}}>No.</th>
                                <th>Kabupaten</th>
                                <th className="text-center align-middle">Provinsi</th>
                                <th className="text-center align-middle" style={{width: "150px"}}>Aksi</th>
                            </tr>
                            </thead>
                            <tbody>
                            {(kabupatenAll.length) ? (
                                kabupatenAll.map((kabupaten, i) => (
                                <tr key={i}>
                                    <th className="text-center align-middle">{i+1}</th>
                                    <td>{kabupaten.nama}</td>
                                    <td className="text-center align-middle">
                                        {
                                            provinsiAll.map(provinsi => {
                                                if (provinsi.id === kabupaten.provinsi_id) {
                                                    return <span key={provinsi.id}>{provinsi.nama}</span>
                                                }
                                            })
                                        }
                                    </td>
                                    <td className="d-flex align-items-center justify-content-center">
                                        <button
                                            className="btn btn-sm btn-warning mx-1"
                                            onClick={() => handleEditButton(kabupaten.id)}
                                        >Edit</button>
                                        <button
                                            className="btn btn-sm btn-danger mx-1"
                                            onClick={() => handleDeleteKabupaten(kabupaten.id)}
                                        >Delete</button>
                                    </td>
                                </tr>
                                ))
                            ) : (
                              <tr>
                                  <td colSpan={4} className="text-center">Tidak Ada Data</td>
                              </tr>
                            )}
                            </tbody>
                        </table>
                    </ul>
                </div>
            )}
            {(showUpdate && kabupaten) && <KabupatenInput input="update" id={id} data={kabupaten} submit={handleUpdateForm} provinsiAll={provinsiAll} close={handleCloseButton} />}
            {(showAdd) && <KabupatenInput input="submit" submit={hanldSubmitForm} provinsiAll={provinsiAll} close={handleCloseButton}/>}
        </div>
    )
}

export default KabupatenIndex;
