import React, {useEffect, useState} from "react";

const KabupatenInput = ({data = null, input, id, submit, close, provinsiAll}) => {
    const [kabupaten, setKabupaten] = useState({
        nama: "",
        provinsi_id: 0
    });

    useEffect(() => {
      if (data) {
          setKabupaten({nama: data.nama, provinsi_id: data.provinsi_id});
      }
    }, [data])

    const handleChangeInput = (e) => {
        setKabupaten({...kabupaten, [e.target.name]: e.target.value});
    }

    return (
        <div>
            <div className="card mx-auto">
                <div className="card-header d-flex justify-content-between align-items-center">
                    <h5 className="m-0 p-0">Kabupaten</h5>
                    <div className="wrapper-button">
                        <button onClick={close} className="btn btn-sm btn-primary">Close</button>
                    </div>
                </div>
                <div className="card-body">
                    <form onSubmit={(e) => {
                        e.preventDefault();
                        (input !== "update") ? submit(kabupaten) : submit(id, kabupaten);
                    }}>
                        <div className="form-group">
                            <label htmlFor="desa">Nama Kabupaten:</label>
                            <input
                                type="text"
                                id="desa"
                                className="form-control"
                                placeholder="Temanggung"
                                name="nama"
                                value={kabupaten.nama}
                                onChange={handleChangeInput}
                            />
                        </div>
                        <div className="form-group">
                            <label htmlFor="desa">Nama Provinsi:</label>
                            <select
                                name="provinsi_id"
                                className="custom-select"
                                value={kabupaten.provinsi_id}
                                onChange={handleChangeInput}>
                                <option>Pilih provinsi</option>
                                {(provinsiAll.length) ? (
                                    provinsiAll.map((provinsi, i) => (
                                        <option key={i} value={provinsi.id}>{provinsi.nama}</option>
                                    ))
                                ) : (
                                    <option>Tidak ada provinsi</option>
                                )}
                            </select>
                        </div>
                        <div className="form-group">
                            <input type="submit" className="btn btn-sm btn-primary btn-block" value="Submit"/>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    )
}

export default KabupatenInput;
