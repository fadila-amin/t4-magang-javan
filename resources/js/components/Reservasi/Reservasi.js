import React from "react";
import {Link} from "react-router-dom";

const Reservasi = () => {
    return (
        <div>
            <div className="card">
                <div className="card-header text-center">
                    Reservasi Film
                </div>
                <div className="card-body">
                    <div className="row">
                        <div className="col-md-6">
                            <Link to="people" className="wrapper-width text-white bg-info py-5 d-flex align-items-center justify-content-center">
                                People
                            </Link>
                        </div>
                        <div className="col-md-6">
                            <Link to="/film" className="wrapper-width text-white bg-success py-5 d-flex align-items-center justify-content-center">
                                Film
                            </Link>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-md-6 mx-auto my-3">
                            <Link to="/pesanfilm" className="wrapper-width text-dark bg-warning py-5 d-flex align-items-center justify-content-center">
                                Reservasi Film
                            </Link>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Reservasi;
