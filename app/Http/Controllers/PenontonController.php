<?php

namespace App\Http\Controllers;

use App\Models\Film;
use App\Models\Penonton;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class PenontonController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $penonton = Penonton::all();
        return response()->json([
            'message' => 'success',
            'data' => $penonton
        ], Response::HTTP_OK);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required|min:2'
        ]);

        try {
            $penonton = Penonton::create($request->all());
            $response = [
                'massage' => 'Penonton telah ditambahkan',
                'data' => $penonton
            ];
            return response()->json($response, Response::HTTP_CREATED);
        } catch (QueryException $e) {
            return response()->json([
                'message' => "Gagal | ".$e->errorInfo
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $penonton = Penonton::findOrFail($id);
        $response = [
            'massage' => 'succes',
            'data' => $penonton,
            'reservasi' => Film::whereIn('id',$penonton->film()->allRelatedIds())->get()
        ];
        return response()->json($response, Response::HTTP_OK);
    }



    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $penonton = Penonton::findOrFail($id);
        $request->validate([
            'nama' => 'required|min:2'
        ]);

        try {
            $penonton->update($request->all());
            $response = [
                'massage' => 'Penonton telah diupdate',
                'data' => $penonton
            ];
            return response()->json($response, Response::HTTP_OK);
        } catch (QueryException $e) {
            return response()->json([
                'message' => "Gagal | ".$e->errorInfo
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $penonton = Penonton::findOrFail($id);

        try {
            $penonton->delete();
            $response = [
                'massage' => 'Penonton telah dihapus'
            ];
            return response()->json($response, Response::HTTP_OK);
        } catch (QueryException $e) {
            return response()->json([
                'message' => "Gagal | ".$e->errorInfo
            ]);
        }
    }
}
