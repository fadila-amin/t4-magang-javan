<?php

namespace App\Http\Controllers;

use App\Models\Film;
use App\Models\Penonton;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class FilmController extends Controller
{
    public function reservasi(Request $request)
    {
        $request->validate([
            'film_id' => 'required|numeric',
            'penonton_id' => 'required|numeric'
        ]);
        $film = Film::find($request->input('film_id'));
        try {
        $film->penonton()->attach($request->input('penonton_id'));
        return response()->json([
            'message' => 'success',
        ], Response::HTTP_OK);
    } catch (QueryException $e) {
        return response()->json([
            'message' => "Gagal | ".$e->errorInfo
        ]);
    }
    }

    public function batal_reservasi(Request $request){
        $request->validate([
            'film_id' => 'required|numeric',
            'penonton_id' => 'required|numeric'
        ]);
        $film = Film::find($request->input('film_id'));
        try {
            $film->penonton()->detach($request->input('penonton_id'));
            return response()->json([
                'message' => 'success',
            ], Response::HTTP_OK);
        } catch (QueryException $e) {
            return response()->json([
                'message' => "Gagal | ".$e->errorInfo
            ]);
        }

    }
    /**
      * Display a listing of the resource.
      *
      * @return \Illuminate\Http\Response
      */
    public function index()
    {
        $film = Film::all();
        return response()->json([
            'message' => 'success',
            'data' => $film
        ], Response::HTTP_OK);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'judul' => 'required|min:2',
            'tiket' => 'required|numeric'

        ]);

        try {
            $film = Film::create($request->all());
            $response = [
                'massage' => 'film telah ditambahkan',
                'data' => $film
            ];
            return response()->json($response, Response::HTTP_CREATED);
        } catch (QueryException $e) {
            return response()->json([
                'message' => "Gagal | ".$e->errorInfo
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $film = Film::findOrFail($id);
        $response = [
            'massage' => 'succes',
            'data' => $film,
            'reservasi' => Penonton::whereIn('id',$film->penonton()->allRelatedIds())->get()
        ];
        return response()->json($response, Response::HTTP_OK);
    }



    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $film = Film::findOrFail($id);
        $request->validate([
            'judul' => 'required|min:2',
            'tiket' => 'required|numeric'
                ]);

        try {
            $film->update($request->all());
            $response = [
                'massage' => 'film telah diupdate',
                'data' => $film
            ];
            return response()->json($response, Response::HTTP_OK);
        } catch (QueryException $e) {
            return response()->json([
                'message' => "Gagal | ".$e->errorInfo
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $film = Film::findOrFail($id);

        try {
            $film->delete();
            $response = [
                'massage' => 'film telah dihapus'
            ];
            return response()->json($response, Response::HTTP_OK);
        } catch (QueryException $e) {
            return response()->json([
                'message' => "Gagal | ".$e->errorInfo
            ]);
        }
    }
}
