<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Film extends Model
{
    protected $table = 'film';
    protected $fillable = ['judul','tiket'];

    public function penonton(){
        return $this->belongsToMany('App\Models\Penonton','reservasi','film_id','penonton_id');
    }
}
